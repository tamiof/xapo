package com.example.xapo.model_layer.network.requests;

import java.util.HashMap;
import java.util.Map;

public class GithubProjectsRequest implements XapoRequestParams {

    private final String QUERY = "q";
    private final String SORT = "sort";
    private final String ORDER = "order";
    private final String PAGE = "page";
    private final String PER_PAGE = "per_page";

    private String q;
    private String sort;
    private String order;
    private int page;
    private int per_page;

    public GithubProjectsRequest(String query, String sort, String order, int page, int perPage) {
        this.q = query;
        this.sort = sort;
        this.order = order;
        this.page = page;
        this.per_page = perPage;
    }

    //region XapoRequestParams
    @Override
    public Map<String, String> getQueryMap() {
        Map<String,String> params = new HashMap<>();

        try {
            params.put(QUERY,q);
            params.put(SORT,sort);
            params.put(ORDER,order);
            params.put(PAGE,String.valueOf(page));
            params.put(PER_PAGE,String.valueOf(per_page));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return params;
    }
    //endregion
}