package com.example.xapo.model_layer.network;

import com.example.xapo.activities.projects_list.adapter.ProjectsListener;
import com.example.xapo.activities.projects_list.ProjectsParams;
import com.example.xapo.model_layer.data.model.GithubProject;
import com.example.xapo.model_layer.network.callbacks.XapoListHttpCallback;
import com.example.xapo.model_layer.network.requests.GithubProjectsRequest;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;

public class NetworkLayerImpl implements NetworkLayer {

    private Gson gson;
    private GithubAPI githubAPI;

    public NetworkLayerImpl(GithubAPI githubAPI, Gson gson) {
        this.githubAPI = githubAPI;
        this.gson = gson;
    }

    @Override
    public void getProjects(ProjectsParams projectsParams, final ProjectsListener projectsListener) {

        GithubProjectsRequest githubProjectsRequest = new GithubProjectsRequest(projectsParams.getLanguage(),projectsParams.getSort(),projectsParams.getOrder(),projectsParams.getPage(),projectsParams.getPerPage());

        githubAPI.getTrendingProjects(githubProjectsRequest.getQueryMap()).enqueue(new XapoListHttpCallback<List<GithubProject>>(gson) {

            @Override
            public void onResponseList(List<GithubProject> projects) {
                projectsListener.projectsLoaded(projects);
            }

            @Override
            public void onFailureList(Call call, Throwable t, int errorCode, String errorMsg) {

            }
        });
    }

}
