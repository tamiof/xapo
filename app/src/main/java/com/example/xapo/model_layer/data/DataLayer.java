package com.example.xapo.model_layer.data;

import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.List;

public interface DataLayer {

    void setProjects(List<GithubProject> projects);

    GithubProject getProjectForId(long id);
}
