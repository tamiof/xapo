package com.example.xapo.model_layer.data.model;

public interface ProjectListable {
    int getViewType();
    long getListableId();
    String getListableName();
    String getListableLanguage();
    int getListableStars();
    int getListableWatchers();
}
