package com.example.xapo.model_layer.translation;

import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.ArrayList;
import java.util.List;

public class TranslationLayerImpl implements TranslationLayer {

    @Override
    public List<ProjectListable> translateProjectsToListables(List<GithubProject> projects) {

        return new ArrayList<ProjectListable>(projects);

    }

    @Override
    public ProjectListable translateProjectToListable(GithubProject project) {
        return project;
    }
}
