package com.example.xapo.model_layer.data;


import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.ArrayList;
import java.util.List;

public class DataLayerImpl implements DataLayer {

    private List<GithubProject> projects = new ArrayList<>();


    @Override
    public void setProjects(List<GithubProject> projects) {
        this.projects.addAll(projects);
    }

    @Override
    public GithubProject getProjectForId(long id) {
        for (GithubProject project : projects) {
            if (project.getId() == id) {
                return project;
            }
        }
        return null;
    }
}
