package com.example.xapo.model_layer.network.requests;

import java.util.Map;

public interface XapoRequestParams {
    Map<String,String> getQueryMap();
}
