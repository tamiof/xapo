package com.example.xapo.model_layer.translation;

import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.List;

public interface TranslationLayer {
    List<ProjectListable> translateProjectsToListables(List<GithubProject> projects);

    ProjectListable translateProjectToListable(GithubProject project);
}
