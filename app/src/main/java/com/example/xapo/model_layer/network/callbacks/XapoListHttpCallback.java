package com.example.xapo.model_layer.network.callbacks;

import com.example.xapo.model_layer.network.BaseResponse;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.xapo.helpers.Constants.UNDEFINED;

public abstract class XapoListHttpCallback<T> implements Callback<BaseResponse> {

    private Class type;
    private Gson gson;
    public XapoListHttpCallback(Gson gson){

        this.gson = gson;
        Type actualTypeArguments = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        type = (Class) ((ParameterizedType) actualTypeArguments).getActualTypeArguments()[0];

    }


    @Override
    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

        try {

            if (isValidResponse(response) && response.isSuccessful()) {

                if (response.body().getItems() != null && response.body().getItems().size() > 0) {

                    JsonArray jsonArray = response.body().getItems();
                    List<T> arrayList = gson.fromJson(jsonArray.toString(),new ListOfJson<T>(type));
                    onResponseList((T)arrayList);

                } else {
                    onResponseList((T) new ArrayList<T>()); //error -> return empty list
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private boolean isValidResponse(Response<BaseResponse> response) {
        return response != null && response.body() != null;
    }

    @Override
    public void onFailure(final Call call, Throwable t) {

        t.printStackTrace();
        onFailureList(call,t,UNDEFINED,"Error");

    }

    public abstract void onResponseList(T response);
    public abstract void onFailureList(Call call, Throwable t, int errorCode, String errorMsg);

}