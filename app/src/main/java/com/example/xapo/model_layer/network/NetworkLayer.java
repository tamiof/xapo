package com.example.xapo.model_layer.network;

import com.example.xapo.activities.projects_list.ProjectsParams;
import com.example.xapo.activities.projects_list.adapter.ProjectsListener;

public interface NetworkLayer {
    void getProjects(ProjectsParams projectsParams, ProjectsListener projectsListener);
}
