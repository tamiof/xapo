package com.example.xapo.model_layer.data.model;

import static com.example.xapo.helpers.Constants.LOADER;
import static com.example.xapo.helpers.Constants.UNDEFINED;

public class ProjectLoader implements ProjectListable {

    //region ProjectListable
    @Override
    public int getViewType() {
        return LOADER;
    }

    @Override
    public long getListableId() {
        return UNDEFINED;
    }

    @Override
    public String getListableName() {
        return ".";
    }

    @Override
    public String getListableLanguage() {
        return ".";
    }

    @Override
    public int getListableStars() {
        return UNDEFINED;
    }

    @Override
    public int getListableWatchers() {
        return UNDEFINED;
    }

    //endregion
}
