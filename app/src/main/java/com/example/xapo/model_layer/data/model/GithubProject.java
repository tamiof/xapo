package com.example.xapo.model_layer.data.model;

import com.google.gson.annotations.SerializedName;

import static com.example.xapo.helpers.Constants.GITHUB_PROJECT;

public class GithubProject implements ProjectListable {

    private long id;
    private String language;
    private String name;
    private int watchers;
    @SerializedName("stargazers_count")
    private int stars;


    public long getId() {
        return id;
    }

    //region ProjectListable
    @Override
    public int getViewType() {
        return GITHUB_PROJECT;
    }

    @Override
    public long getListableId() {
        return id;
    }

    @Override
    public String getListableName() {
        return name;
    }

    @Override
    public String getListableLanguage() {
        return language;
    }

    @Override
    public int getListableStars() {
        return stars;
    }

    @Override
    public int getListableWatchers() {
        return watchers;
    }

    //endregion
}
