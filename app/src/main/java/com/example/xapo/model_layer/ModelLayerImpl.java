package com.example.xapo.model_layer;

import com.example.xapo.activities.projects_list.adapter.ListableProjectsListener;
import com.example.xapo.activities.projects_list.adapter.ProjectsListener;
import com.example.xapo.activities.projects_list.ProjectsParams;
import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.model_layer.data.DataLayer;
import com.example.xapo.model_layer.data.model.GithubProject;
import com.example.xapo.model_layer.network.NetworkLayer;
import com.example.xapo.model_layer.translation.TranslationLayer;

import java.util.List;

//helps decoupling code
public class ModelLayerImpl implements ModelLayer {

    private NetworkLayer networkLayer;
    private DataLayer dataLayer;
    private TranslationLayer translationLayer;

    public ModelLayerImpl(NetworkLayer networkLayer, DataLayer dataLayer, TranslationLayer translationLayer) {
        this.networkLayer = networkLayer;
        this.dataLayer = dataLayer;
        this.translationLayer = translationLayer;
    }


    @Override
    public void getListableProjects(ProjectsParams projectsParams, final ListableProjectsListener listableProjectsListener) {
        networkLayer.getProjects(projectsParams, new ProjectsListener() {
            @Override
            public void projectsLoaded(List<GithubProject> projects) {
                setProjects(projects);
                listableProjectsListener.projectsLoaded(translationLayer.translateProjectsToListables(projects));
            }
        });
    }


    @Override
    public void setProjects(List<GithubProject> projects) {
        dataLayer.setProjects(projects);
    }

    @Override
    public ProjectListable getProjectForId(long id) {
        return translationLayer.translateProjectToListable(dataLayer.getProjectForId(id));
    }
}
