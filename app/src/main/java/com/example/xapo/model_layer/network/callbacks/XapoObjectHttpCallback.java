package com.example.xapo.model_layer.network.callbacks;

import com.example.xapo.model_layer.network.BaseResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.ParameterizedType;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class XapoObjectHttpCallback<T> implements Callback<BaseResponse> {

    private Class type;
    private Gson gson;

    public XapoObjectHttpCallback(){

        type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        gson = new GsonBuilder().create();
    }


    @Override
    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

        try {

            if (isValidResponse(response) && response.isSuccessful()) {

                onResponseObject((T)gson.fromJson(response.toString(),type));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private boolean isValidResponse(Response<BaseResponse> response) {
        return response != null && response.body() != null;
    }

    @Override
    public void onFailure(final Call call, Throwable t) {

        t.printStackTrace();
        onFailureObject(call,t,"-2","Error");

    }

    public abstract void onResponseObject(T response);
    public abstract void onFailureObject(Call call, Throwable t, String errorCode, String errorMsg);

}