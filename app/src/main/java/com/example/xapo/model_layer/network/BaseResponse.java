package com.example.xapo.model_layer.network;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("total_count")
    private int totalCount;
    @SerializedName("incomplete_results")
    private boolean incompleteResults;
    private JsonArray items;

    public int getTotalCount() {
        return totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public JsonArray getItems() {
        return items;
    }
}
