package com.example.xapo.model_layer.network;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GithubAPI {

    //call type should be the base class of the response - the one specified in the callback implementation class
    @GET("/search/repositories")
    Call<BaseResponse> getTrendingProjects(@QueryMap (encoded = true) Map<String,String> params);

}
