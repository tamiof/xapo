package com.example.xapo.model_layer;

import com.example.xapo.activities.projects_list.ProjectsParams;
import com.example.xapo.activities.projects_list.adapter.ListableProjectsListener;
import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.List;

public interface ModelLayer {
    void getListableProjects(ProjectsParams projectsParams, ListableProjectsListener listableProjectsListener);

    void setProjects(List<GithubProject> projects);

    ProjectListable getProjectForId(long id);
}
