package com.example.xapo;

import android.app.Application;

public class XapoApplication extends Application {

    private DependencyRegistry registry;

    @Override
    public void onCreate() {
        super.onCreate();

        //forces initialization
        registry = DependencyRegistry.shared;
    }
}
