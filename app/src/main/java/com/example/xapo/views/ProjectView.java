package com.example.xapo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.xapo.R;
import com.example.xapo.model_layer.data.model.ProjectListable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectView extends BaseView {

    private ProjectViewListener projectViewListener;
    private ProjectListable project;

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_stars)
    TextView tvStars;
    @BindView(R.id.tv_wachers)
    TextView tvWachers;
    @BindView(R.id.tv_language)
    TextView tvLanguage;

    public ProjectView(Context context) {
        super(context);
        init(context,null,0);
    }

    public ProjectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs,0);
    }

    public ProjectView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        View view = LayoutInflater.from(context).inflate(R.layout.project_view, this);
        ButterKnife.bind(view);

    }

    public void setProject(final ProjectListable projectListable) {

        this.project = projectListable;

        if (project != null) {
            tvName.setText(project.getListableName());
            tvStars.setText(String.valueOf(project.getListableStars()));
            tvWachers.setText(String.valueOf(project.getListableWatchers()));
            tvLanguage.setText(project.getListableLanguage());
        }

        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (projectViewListener != null) {
                    projectViewListener.projectViewClick(projectListable);
                }

            }
        });
    }

    public void setProjectViewListener(ProjectViewListener projectViewListener) {
        this.projectViewListener = projectViewListener;
    }

    public interface ProjectViewListener {
        void projectViewClick(ProjectListable projectListable);
    }

}
