package com.example.xapo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class BaseView extends FrameLayout {
    public BaseView(Context context) {
        this(context, null, 0);
    }

    public BaseView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
