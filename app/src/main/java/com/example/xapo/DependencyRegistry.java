package com.example.xapo;

import android.os.Bundle;

import com.example.xapo.activities.project_details.ProjectDetailsActivity;
import com.example.xapo.activities.project_details.ProjectDetailsPresenter;
import com.example.xapo.activities.project_details.ProjectDetailsPresenterImpl;
import com.example.xapo.activities.projects_list.GithubProjectsActivity;
import com.example.xapo.activities.projects_list.GithubProjectsPresenter;
import com.example.xapo.activities.projects_list.GithubProjectsPresenterImpl;
import com.example.xapo.activities.projects_list.ProjectsParams;
import com.example.xapo.coordinators.RootCoordinator;
import com.example.xapo.model_layer.data.DataLayer;
import com.example.xapo.model_layer.data.DataLayerImpl;
import com.example.xapo.model_layer.ModelLayer;
import com.example.xapo.model_layer.ModelLayerImpl;
import com.example.xapo.model_layer.network.GithubAPI;
import com.example.xapo.model_layer.translation.TranslationLayer;
import com.example.xapo.model_layer.translation.TranslationLayerImpl;
import com.example.xapo.model_layer.network.NetworkLayer;
import com.example.xapo.model_layer.network.NetworkLayerImpl;
import com.google.gson.Gson;

import java.util.NoSuchElementException;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.xapo.helpers.Constants.BASE_URL;
import static com.example.xapo.helpers.Constants.PROJECT_ID_KEY;

public class DependencyRegistry {

    public static DependencyRegistry shared = new DependencyRegistry();

    private NetworkLayer networkLayer;
    private DataLayer dataLayer;
    private TranslationLayer translationLayer;
    private ModelLayer modelLayer;
    private RootCoordinator rootCoordinator;

    //region External Dependencies
    private Gson gson;
    private Retrofit retrofit;
    private GithubAPI githubAPI;
    //endregion

    public DependencyRegistry() {

        rootCoordinator = new RootCoordinator();

        gson = new Gson();
        retrofit = makeRetrofit();
        githubAPI = retrofit.create(GithubAPI.class);

        networkLayer = new NetworkLayerImpl(githubAPI,gson);
        dataLayer = new DataLayerImpl();
        translationLayer = new TranslationLayerImpl();
        modelLayer = new ModelLayerImpl(networkLayer,dataLayer,translationLayer);

    }

    //region Makers
    private Retrofit makeRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(makeOKHttpClient())
                .build();
    }
    private OkHttpClient makeOKHttpClient() {
        return new OkHttpClient.Builder().build();
    }
    //endregion

    //region Inject
    public void inject(GithubProjectsActivity activity) {
        GithubProjectsPresenter presenter = new GithubProjectsPresenterImpl(modelLayer, new ProjectsParams(1));
        activity.configure(presenter, rootCoordinator);
    }

    public void inject(ProjectDetailsActivity activity, Bundle bundle) throws NoSuchElementException {

        long projectId = idFromBundle(bundle);
        ProjectDetailsPresenter presenter = new ProjectDetailsPresenterImpl(projectId, modelLayer);
        activity.configureWith(presenter);
    }
    //endregion

    //region Helper Methods

    private long idFromBundle(Bundle bundle) {

        if(bundle == null) {
            throw new NoSuchElementException("Unable to get project id from bundle");
        }

        long projectId = bundle.getLong(PROJECT_ID_KEY);
        if(projectId == 0) {
            throw new NoSuchElementException("Unable to get project id from bundle");
        }

        return projectId;
    }
    //endregion
}
