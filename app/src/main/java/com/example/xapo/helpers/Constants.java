package com.example.xapo.helpers;

public class Constants {

    public static final int UNDEFINED = -1;
    public static final int GITHUB_PROJECT = 1;
    public static final int LOADER = 2;

    public static final String PROJECT_ID_KEY = "projectIdKey";

    public static final String BASE_URL = "https://api.github.com";
}
