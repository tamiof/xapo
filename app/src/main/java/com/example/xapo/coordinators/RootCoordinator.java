package com.example.xapo.coordinators;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.xapo.activities.project_details.ProjectDetailsActivity;

import static com.example.xapo.helpers.Constants.PROJECT_ID_KEY;

public class RootCoordinator {

    public void handleProjectTap(Context context, long projectId) {

        Bundle bundle = new Bundle();
        bundle.putLong(PROJECT_ID_KEY, projectId);

        Intent intent = new Intent(context, ProjectDetailsActivity.class);
        intent.putExtras(bundle);

        context.startActivity(intent);

    }
}
