package com.example.xapo.activities.projects_list;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.example.xapo.DependencyRegistry;
import com.example.xapo.R;
import com.example.xapo.activities.projects_list.adapter.ListableProjectsListener;
import com.example.xapo.activities.projects_list.adapter.ProjectVerticalSpaceItemDecoration;
import com.example.xapo.activities.projects_list.adapter.XapoAdapter;
import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.coordinators.RootCoordinator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GithubProjectsActivity extends AppCompatActivity implements XapoAdapter.ListableItemClickListener {

    private GithubProjectsPresenter presenter;
    private RootCoordinator rootCoordinator;
    private XapoAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fl_pb)
    FrameLayout flProgressBar;
    @BindView(R.id.fl_parent)
    FrameLayout flParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_github_projects);
        attachUI();
        DependencyRegistry.shared.inject(this);

        presenter.getListableProjects(new ListableProjectsListener() {
            @Override
            public void projectsLoaded(List<ProjectListable> projects) {

                flParent.removeView(flProgressBar);

                adapter = new XapoAdapter(GithubProjectsActivity.this, projects, GithubProjectsActivity.this);
                recyclerView.setAdapter(adapter);

                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                        int totalItem = linearLayoutManager.getItemCount();
                        int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                        loadMoreIfNeeded(totalItem, lastVisibleItem);
                    }

                    private void loadMoreIfNeeded(int totalItem, int lastVisibleItem) {
                        if (!presenter.isLoadingItems() && lastVisibleItem == totalItem - 1) {

                            presenter.setLoadingItems(true);
                            adapter.presentLoader();

                            presenter.getNextListableProjects(new ListableProjectsListener() {
                                @Override
                                public void projectsLoaded(List<ProjectListable> projects) {
                                    presenter.setLoadingItems(false);
                                    adapter.hideLoader();
                                    adapter.updateItems(projects);
                                }
                            });

                        }
                    }
                });
            }
        });
    }

    public void configure(GithubProjectsPresenter presenter, RootCoordinator rootCoordinator) {
        this.presenter = presenter;
        this.rootCoordinator = rootCoordinator;
    }

    //region UI
    private void attachUI() {
        ButterKnife.bind(this);
        initRecyclerView();
    }

    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new ProjectVerticalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.project_seperator_height)));
        recyclerView.setItemAnimator(null);
    }
    //endregion

    //region Click Listener
    @Override
    public void onProjectClick(ProjectListable project) {
        gotoProjectDetails(project.getListableId());
    }
    //endregion

    //region Navigation

    private void gotoProjectDetails(long projectId) {

        rootCoordinator.handleProjectTap(this,projectId);
    }

    //endregion

}
