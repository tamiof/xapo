package com.example.xapo.activities.projects_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.xapo.activities.projects_list.adapter.viewholders.BaseViewHolder;
import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.activities.projects_list.adapter.viewholders.ViewHoldersUtil;
import com.example.xapo.model_layer.data.model.ProjectLoader;

import java.util.List;

public class XapoAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<ProjectListable> mList;
    private LayoutInflater mInflator;
    private ListableItemClickListener listableItemClickListener;

    public XapoAdapter(Context context, List<ProjectListable> list, ListableItemClickListener listableItemClickListener) {
        this.mList = list;
        this.mInflator = LayoutInflater.from(context);
        this.listableItemClickListener = listableItemClickListener;
    }

    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHoldersUtil.getViewHolderByViewType(mInflator,parent,viewType, listableItemClickListener);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(mList.get(position));
    }


    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getViewType();
    }

    public int getItemCount() {
        return mList.size();
    }

    public void presentLoader() {
        mList.add(new ProjectLoader());
        notifyItemInserted(mList.size()-1);
    }

    public void hideLoader() {
        mList.remove(mList.size()-1);
        notifyItemRemoved(mList.size()-1);
    }

    public void updateItems(List<ProjectListable> projectListables) {

        final int positionStart = mList.size() + 1;
        mList.addAll(projectListables);
        notifyItemRangeInserted(positionStart, projectListables.size());

    }

    public interface ListableItemClickListener {
        void onProjectClick(ProjectListable project);
    }
}