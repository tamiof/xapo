package com.example.xapo.activities.projects_list.adapter;

import com.example.xapo.model_layer.data.model.GithubProject;

import java.util.List;

public interface ProjectsListener {
    void projectsLoaded(List<GithubProject> projects);
}
