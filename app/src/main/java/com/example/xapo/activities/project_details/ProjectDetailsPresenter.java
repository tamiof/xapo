package com.example.xapo.activities.project_details;

import com.example.xapo.model_layer.data.model.ProjectListable;

public interface ProjectDetailsPresenter {
    ProjectListable getProject();
}
