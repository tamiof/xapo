package com.example.xapo.activities.projects_list.adapter.viewholders;

import android.view.View;

import com.example.xapo.R;
import com.example.xapo.activities.projects_list.adapter.XapoAdapter;
import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.views.ProjectView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectViewHolder extends BaseViewHolder<ProjectListable> implements ProjectView.ProjectViewListener {

    private XapoAdapter.ListableItemClickListener listableItemClickListener;
    @BindView(R.id.v_project)
    ProjectView projectView;

    public ProjectViewHolder(View itemView, final XapoAdapter.ListableItemClickListener listableItemClickListener) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.listableItemClickListener = listableItemClickListener;
    }

    @Override
    public void bind(ProjectListable project) {
        projectView.setProject(project);
        projectView.setProjectViewListener(this);
    }


    @Override
    public void projectViewClick(ProjectListable projectListable) {
        listableItemClickListener.onProjectClick(projectListable);
    }


}
