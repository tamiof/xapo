package com.example.xapo.activities.projects_list.adapter.viewholders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.xapo.R;
import com.example.xapo.activities.projects_list.adapter.XapoAdapter;

import static com.example.xapo.helpers.Constants.GITHUB_PROJECT;
import static com.example.xapo.helpers.Constants.LOADER;

public class ViewHoldersUtil {
    public static BaseViewHolder getViewHolderByViewType(LayoutInflater mInflator, ViewGroup parent, int viewType, XapoAdapter.ListableItemClickListener listableItemClickListener) {

        switch (viewType) {
            case GITHUB_PROJECT:
                return new ProjectViewHolder(mInflator.inflate(R.layout.project_view_holder,parent,false), listableItemClickListener);
            case LOADER:
                return new LoaderViewHolder(mInflator.inflate(R.layout.loader_view_holder,parent,false));
        }
        return new GenericViewHolder(mInflator.inflate(R.layout.item_generic,parent,false));
    }
}
