package com.example.xapo.activities.projects_list.adapter;

import com.example.xapo.model_layer.data.model.ProjectListable;

import java.util.List;

public interface ListableProjectsListener {
    void projectsLoaded(List<ProjectListable> projects);
}
