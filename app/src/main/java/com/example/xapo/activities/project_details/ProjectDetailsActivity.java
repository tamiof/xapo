package com.example.xapo.activities.project_details;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.xapo.DependencyRegistry;
import com.example.xapo.R;
import com.example.xapo.views.ProjectView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectDetailsActivity extends AppCompatActivity {

    private ProjectDetailsPresenter presenter;

    @BindView(R.id.v_project)
    ProjectView projectView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);
        attachUI();
        DependencyRegistry.shared.inject(this,getIntent().getExtras());
        updateUI();
    }

    private void attachUI() {
        ButterKnife.bind(this);
    }

    private void updateUI() {
        projectView.setProject(presenter.getProject());
    }

    public void configureWith(ProjectDetailsPresenter presenter) {
        this.presenter = presenter;
    }
}
