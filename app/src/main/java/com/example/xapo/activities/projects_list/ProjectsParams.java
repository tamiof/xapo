package com.example.xapo.activities.projects_list;

public class ProjectsParams {

    private String language;
    private String sort;
    private String order;
    private int page;
    private int perPage;

    public ProjectsParams(int page) {
        this.language = "language:kotlin";
        this.sort = "stars";
        this.order = "desc";
        this.page = page;
        this.perPage = 10;
    }

    public String getLanguage() {
        return language;
    }

    public String getSort() {
        return sort;
    }

    public String getOrder() {
        return order;
    }

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public ProjectsParams setPage(int page) {
        this.page = page;
        return this;
    }
}
