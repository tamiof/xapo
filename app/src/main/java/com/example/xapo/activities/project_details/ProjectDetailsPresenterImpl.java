package com.example.xapo.activities.project_details;

import com.example.xapo.model_layer.data.model.ProjectListable;
import com.example.xapo.model_layer.ModelLayer;

public class ProjectDetailsPresenterImpl implements ProjectDetailsPresenter {

    private ProjectListable project;

    public ProjectDetailsPresenterImpl(long id, ModelLayer modelLayer) {
        this.project = modelLayer.getProjectForId(id);
    }

    @Override
    public ProjectListable getProject() {
        return project;
    }
}
