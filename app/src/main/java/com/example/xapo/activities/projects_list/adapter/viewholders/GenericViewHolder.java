package com.example.xapo.activities.projects_list.adapter.viewholders;

import android.view.View;

import com.example.xapo.model_layer.data.model.ProjectListable;

import butterknife.ButterKnife;

public class GenericViewHolder extends BaseViewHolder<ProjectListable> {


    public GenericViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    @Override
    public void bind(ProjectListable projectListable) {
    }
}