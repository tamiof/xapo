package com.example.xapo.activities.projects_list.adapter.viewholders;

import android.view.View;

import com.example.xapo.model_layer.data.model.ProjectListable;

public class LoaderViewHolder extends BaseViewHolder<ProjectListable> {

    public LoaderViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(ProjectListable projectListable) {

    }


}
