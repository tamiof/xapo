package com.example.xapo.activities.projects_list;

import com.example.xapo.activities.projects_list.adapter.ListableProjectsListener;

public interface GithubProjectsPresenter {
    void getListableProjects(ListableProjectsListener projectsListener);

    void getNextListableProjects(ListableProjectsListener projectsListener);

    boolean isLoadingItems();

    void setLoadingItems(boolean loadingItems);
}
