package com.example.xapo.activities.projects_list;

import com.example.xapo.activities.projects_list.adapter.ListableProjectsListener;
import com.example.xapo.model_layer.ModelLayer;

public class GithubProjectsPresenterImpl implements GithubProjectsPresenter {

    private ModelLayer modelLayer;

    private int currentPage = 1;
    private ProjectsParams projectsParams;
    private boolean isLoadingItems = false;

    public GithubProjectsPresenterImpl(ModelLayer modelLayer, ProjectsParams projectsParams) {
        this.modelLayer = modelLayer;
        this.projectsParams = projectsParams;
    }

    @Override
    public void getListableProjects(final ListableProjectsListener projectsListener) {
        modelLayer.getListableProjects(projectsParams.setPage(currentPage++), projectsListener);
    }

    @Override
    public void getNextListableProjects(final ListableProjectsListener projectsListener) {
        getListableProjects(projectsListener);
    }

    @Override
    public boolean isLoadingItems() {
        return isLoadingItems;
    }

    @Override
    public void setLoadingItems(boolean loadingItems) {
        isLoadingItems = loadingItems;
    }
}
